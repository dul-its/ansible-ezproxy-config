# Group Variables

Open the `all.yml` file in order to edit a subset of configurable options:  
  
* `max_virtual_hosts`  
* `max_session`  
* `log_user` (true/false)  
* `audit`  
* Intrusion settings - global, user and IP specific  
* Usage Limit settings  
* SHIBUSER section: Selected variable(s)  
