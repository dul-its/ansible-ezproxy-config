# Deployment

## Running Playbook / Deploying EZProxy Config Files
First, SSH into a server that is running the Ansible service.  Currently, such a VM is located at "lib-automation-01".  
If you're running a MacOS shell:  
`ssh lib-automation-01.oit.duke.edu`  
  
Next, clone the project:  
`git clone git@gitlab.oit.duke.edu:dul-its/ansible-ezproxy-config.git`  
  
While the files mostly contain "static" settings, there are some variables located at:
* `group_vars/all.yml`
* Test inventory (test)
* Production inventory (production)
  
**Run the playbook:**  
`ansible-playbook -i <test|production|some_other_inventory_you_have_defined> deploy.yml`  
  
If all goes well, the server configuration settings will be deployed and the EZProxy service will have been restarted.  

**Happy Automation!**

