# EZPROXY CONFIGURATION - DUL ITS

### Preamble
Supersedes legacy SVN repository for managing the configuration files related to the EZProxy service.  Uses Ansible to manage the files.  

#### Note about configs & stanzas
This repository project aims to only manage the configuration, server-specific settings related to EZProxy.  
  
The stanza definitions, such as:
```text
TITLE   Some Database To Proxy
URL     http://www.source-url.com/their/resource
DJ      source-url.com
HJ      www.source-url.com
```
...are managed by **Rubenstein Technical Services** under a separate Gitlab repository. 

## More about Stanzas & RedirectSafe

EZProxy has a configuration option, `RedirectSafe <domain or host>`, that is recommended to be used sparingly.  
  
Currently, it is used to allow redirects to Duke-specific domains or hosts that aren't proxied (by way of stanza definition).  
`RedirectSafe *.duke.edu`

## Changes & Documentation

It is strongly recommended that configuration changes be dictated by:
* Stakeholder-approved change
* ServiceNow ticket
* Other approved ticketing facility (Ask Tech?)

Changes to files should be documented with a date and author, and include a ServiceNow ticket number (where necessary).  
  
## What (Files) To Change

### Non-Core Services Personnel

From within this project directory, navigate to the "group_vars" folder, then:
* click `all.yml`
* Locate and click/tap the Edit button

There is more information about what variables are configurable in the README file located under "group_vars"

### Core Services Personnel

In addition to "group_vars", there are files under the "templates" directory that contain other configurable settings, not 
available in the "group_vars" directory.

## Deployment Strategy

ITS Core Services will maintain ownership of the deployment process for these files.  To that end, all changes will be 
committed against the **refactor** branch.  Core Services will manage the merge process into the Master branch, and 
initiate deployment on the appropriate server(s).
  
